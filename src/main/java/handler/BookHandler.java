package handler;

import helper.StopWatch;
import service.LibraryService;
import connection.SetupServer;
import model.Book;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.logging.Logger;

public class BookHandler extends Thread {

    private final static Logger LOGGER = Logger.getLogger(SetupServer.class.getName());
    private static Socket clientDialog;
    private final LibraryService libraryService;

    private ObjectOutputStream output;
    private ObjectInputStream input;

    public BookHandler(Socket client) {
        clientDialog = client;
        this.libraryService = new LibraryService();
    }

    @Override
    public void run() {
        try {
            StopWatch sw = new StopWatch();

            sw.start();
            output = new ObjectOutputStream(clientDialog.getOutputStream());
            input = new ObjectInputStream(clientDialog.getInputStream());
            output.flush();
            sw.stop();
            LOGGER.info("---------------------------");
            LOGGER.info("start handler " + sw.getElapsedTimeSecs());

            String message = "";
            do {
                if (clientDialog.isClosed()) {
                    break;
                }

                try {
                    message = (String) input.readObject();
                    LOGGER.info("CLIENT: " + message);
                    LOGGER.info("SERVER: Processing request");

                    sw.start();
                    job(message);
                    sw.stop();
                    LOGGER.info("---------------------------");
                    LOGGER.info("message " + message + " " + sw.getElapsedTimeSecs());

                    continue;
                } catch (ClassNotFoundException | IOException classNotFoundException) {
                    LOGGER.severe("Error occured while processing CLIENT's message!");
                }
            } while (!message.equals("QUIT"));
        } catch (IOException e) {
        }
    }

    private void job(String message) throws IOException {
        String[] preparedMessage = processMessage(message);

        switch (preparedMessage[0]) {
            case "ALL":
                List<Book> books = libraryService.getAll(Integer.parseInt(preparedMessage[1]));
                output.writeObject(books);
                output.flush();
                break;

            case "LOGIN":
                boolean isCorrect = libraryService.login(preparedMessage[1], preparedMessage[2]);
                LOGGER.info("SERVER: Client's login attempt: " + isCorrect);

                output.writeObject(isCorrect);
                output.flush();
                break;

            case "ID":
                int id = libraryService.getPersonById(preparedMessage[1]);

                output.writeObject(id);
                output.flush();
                break;

            case "BORROW":
                try {
                    libraryService.borrowBook(Integer.parseInt(preparedMessage[1]), Integer.parseInt(preparedMessage[2]));

                    LOGGER.info(
                            "SERVER: Client has borrowed the book "
                                    + preparedMessage[2]
                    );
                } catch (Exception e) {
                    LOGGER.severe("SERVER: Error while borrowing");
                }

                break;

            case "RETURN":
                libraryService.returnBook(Integer.parseInt(preparedMessage[1]), Integer.parseInt(preparedMessage[2]));
                break;

            case "QUIT":
                LOGGER.info("SERVER: Disconnecting client: " + clientDialog.getInetAddress());
                output.close();
                input.close();
                clientDialog.close();

                break;
        }
    }

    private String[] processMessage(String message) {
        String[] messageArr = message.split(" ");

        return messageArr;
    }
}
