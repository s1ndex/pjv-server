package entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "author", schema = "public", catalog = "db19_shcheden")
public class AuthorEntity {
    private int authorId;
    private PersonEntity personByAuthorId;
    private Collection<BookEntity> books;

    @Id
    @Column(name = "author_id", nullable = false)
    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    @Override
    public String toString() {
        return "AuthorEntity{" +
                "authorId=" + authorId +
                ", personByAuthorId=" + personByAuthorId +
                ", books=" + books +
                '}';
    }

    @ManyToMany(mappedBy = "authors")
    public Collection<BookEntity> getBooks() {
        return books;
    }

    public void setBooks(Collection<BookEntity> books) {
        this.books = books;
    }

    @OneToOne
    @JoinColumn(name = "author_id", referencedColumnName = "person_id", nullable = false)
    public PersonEntity getPersonByAuthorId() {
        return personByAuthorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthorEntity that = (AuthorEntity) o;
        return authorId == that.authorId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorId);
    }

    public void setPersonByAuthorId(PersonEntity personByAuthorId) {
        this.personByAuthorId = personByAuthorId;
    }

//    @OneToMany(mappedBy = "authorByAuthorId")
//    public Collection<AuthorshipEntity> getAuthorshipsByAuthorId() {
//        return authorshipsByAuthorId;
//    }
//
//    public void setAuthorshipsByAuthorId(Collection<AuthorshipEntity> authorshipsByAuthorId) {
//        this.authorshipsByAuthorId = authorshipsByAuthorId;
//    }
}
