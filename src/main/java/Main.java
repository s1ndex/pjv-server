import connection.SetupServer;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        /*LibraryService libraryService = new LibraryService();
        List<Book> b = libraryService.getAll(7);
        libraryService.borrowBook(7,7);
        List<Book> bb = libraryService.getAll(7);
*/

        SetupServer server = new SetupServer();
        server.run();
    }
}