package repository.abstraction;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Generic Dao object
 *
 * @param <E>
 */
public abstract class CRUDRepository<E> {

    /**
     * Entity manager
     */
    protected EntityManager transactionManager;

    /**
     * Entity class passed in implementing constructor
     */
    protected Class<E> clazz;

    public CRUDRepository(EntityManager transactionManager, Class<E> clazz) {
        this.transactionManager = transactionManager;
        this.clazz = clazz;
    }

    /**
     * Creates row in database
     *
     * @param entity entity to be created
     */
    public E create(E entity) {
        transactionManager.persist(entity);
        return entity;
    }

    /**
     * Finds row in database by id
     *
     * @param id submitted id
     * @return entity object
     */
    public E findOne(int id) {
        return transactionManager.find(clazz, id);
    }

    /**
     * Maps all rows in table to entities
     *
     * @return list of entities
     */
    public List<E> getAll() {
        return transactionManager.createQuery("from " + clazz.getName(), clazz).getResultList();
    }

    /**
     * Updates row in database
     *
     * @param entity entity to be updated
     */
    public void update(E entity) {
        transactionManager.merge(entity);
    }

    /**
     * Deleting row in database
     *
     * @param entity entity to be deleted
     */
    public void delete(E entity) {
        transactionManager.remove(entity);
    }

    /**
     * Deleting row from database by id
     *
     * @param entityId id of entity
     */
    public void deleteById(int entityId) {
        E entity = findOne(entityId);
        delete(entity);
    }
}
