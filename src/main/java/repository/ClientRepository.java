package repository;

import entity.ClientEntity;
import repository.abstraction.CRUDRepository;

import javax.persistence.EntityManager;

public class ClientRepository extends CRUDRepository<ClientEntity> {
    public ClientRepository(EntityManager transactionManager) {
        super(transactionManager, ClientEntity.class);
    }
}
