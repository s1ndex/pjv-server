package repository;

import entity.PersonEntity;
import repository.abstraction.CRUDRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class PersonRepository extends CRUDRepository<PersonEntity> {
    public PersonRepository(EntityManager transactionManager) {
        super(transactionManager, PersonEntity.class);
    }

    /**
     * Checking correctness of user's login data
     *
     * @param username username
     * @param password password
     * @return true if data is ok, otherwise it's false
     */
    public boolean isLoginDataCorrect(String username, String password) {
        String check = getPswdByUsername(username);

        if (check != null && check.equals(password)) {
            return true;
        }

        return false;
    }

    /**
     * Getting password by Client's username
     *
     * @param username username
     * @return password if user exists, null otherwise
     */
    public String getPswdByUsername(String username) {
        TypedQuery<PersonEntity> query = transactionManager.createQuery(
                "SELECT p FROM PersonEntity p WHERE (p.username = :username)",
                clazz
        );
        query.setParameter("username", username);
        List<PersonEntity> list = query.getResultList();

        if (list.isEmpty()) {
            return null;
        }

        return list.get(0).getPasswd();
    }

    /**
     * Getting Client's id by his username
     *
     * @param username username
     * @return Client's id if it exists, -1 otherwise
     */
    public int getPersonIdByUsername(String username) {
        TypedQuery<PersonEntity> query = transactionManager.createQuery(
                "SELECT p FROM PersonEntity p WHERE (p.username = :username)",
                clazz
        );
        query.setParameter("username", username);
        List<PersonEntity> list = query.getResultList();

        if (list.isEmpty()) {
            return -1;
        }

        return list.get(0).getPersonId();
    }
}
