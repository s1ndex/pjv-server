package IntegrationTests;

import connection.SetupServer;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class SetupServerTest {

    @Test
    void StartCloseServerTest() throws IOException {
        // Arrange
        SetupServer server = new SetupServer();

        // Act
        server.serverSetup();
        server.shutDownServer();

        // Assert
        assertTrue(server.getServer().isClosed());
    }

}