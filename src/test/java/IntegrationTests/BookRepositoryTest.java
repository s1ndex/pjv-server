package IntegrationTests;

import entity.BookEntity;
import org.junit.jupiter.api.Test;
import service.LibraryService;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static Data.DataGenerator.*;
import static org.mockito.Mockito.*;

class BookRepositoryTest {

    LibraryService libraryService = new LibraryService();

    @Test
    void BookRepositoryCRUDTest() {
        // Arrange
        BookEntity bookEntity = new BookEntity();

        bookEntity.setIsbn(GenerateString(10));
        bookEntity.setPhysicalSn(GenerateString(10));
        bookEntity.setBookshelf(GenerateString(10));
        bookEntity.setBookName(GenerateString(20));

        // Act
        BookEntity expectedEntity = libraryService.executeInTransaction(() -> {
            int bookId = libraryService.getBookRepository().create(bookEntity).getBookId();
            BookEntity tmp = libraryService.getBookRepository().findOne(bookId);
            tmp.setBookName(GenerateString(25));
            libraryService.getBookRepository().update(tmp);
            libraryService.getBookRepository().delete(tmp);

            return tmp;
        });

        // Assert
        assertEquals(bookEntity.getIsbn(), expectedEntity.getIsbn());
        assertEquals(bookEntity.getPhysicalSn(), expectedEntity.getPhysicalSn());
        assertEquals(bookEntity.getBookshelf(), expectedEntity.getBookshelf());
        assertTrue(expectedEntity.getBookName().equals(bookEntity.getBookName()));
    }

}