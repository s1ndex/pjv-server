package IntegrationTests;

import entity.BorrowedBookEntity;
import org.junit.jupiter.api.Test;
import service.LibraryService;

import java.sql.Date;
import java.time.LocalDate;

import static Data.DataGenerator.GenerateInt;
import static org.junit.jupiter.api.Assertions.*;

class BorrowRepositoryTest {

    @Test
    void BorrowRepositoryCRUDTest() {
        // Arrange
        LibraryService libraryService = new LibraryService();
        BorrowedBookEntity borrowedBookEntity = new BorrowedBookEntity();

        borrowedBookEntity.setClientId(7);
        borrowedBookEntity.setBookId(10);
        borrowedBookEntity.setBorrowDt(Date.valueOf(LocalDate.now()));
        borrowedBookEntity.setReturnDt(null);

        // Act
        BorrowedBookEntity expectedEntity = libraryService.executeInTransaction(() -> {
            int borrowId = libraryService.getBorrowRepository().create(borrowedBookEntity).getBorrowId();
            BorrowedBookEntity tmp = libraryService.getBorrowRepository().findOne(borrowId);
            tmp.setReturnDt(Date.valueOf(LocalDate.now()));
            libraryService.getBorrowRepository().update(tmp);
            libraryService.getBorrowRepository().delete(tmp);

            return tmp;
        });

        // Assert
        assertEquals(borrowedBookEntity.getClientId(), expectedEntity.getClientId());
        assertEquals(borrowedBookEntity.getBookId(), expectedEntity.getBookId());
        assertEquals(borrowedBookEntity.getBorrowDt(), expectedEntity.getBorrowDt());
        assertTrue(expectedEntity.getReturnDt() == borrowedBookEntity.getReturnDt());
    }

    @Test
    void GetBorrowEntityNullTest() {
        // Arrange
        LibraryService libraryService = new LibraryService();

        // Act
        BorrowedBookEntity borrowedBookEntity = libraryService.getBorrowRepository().getBorrowEntity(
                GenerateInt(), GenerateInt()
        );

        // Assert
        assertNull(borrowedBookEntity);
    }
}