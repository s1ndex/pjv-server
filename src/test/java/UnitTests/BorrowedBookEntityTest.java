package UnitTests;

import entity.BookEntity;
import entity.BorrowedBookEntity;
import entity.ClientEntity;
import org.junit.jupiter.api.Test;

import java.sql.Date;

import static org.junit.jupiter.api.Assertions.*;
import static Data.DataGenerator.*;
import static org.mockito.Mockito.*;

class BorrowedBookEntityTest {

    @Test
    void SetBorrowedEntityTest() {
        // Arrange
        BorrowedBookEntity testBorrow = new BorrowedBookEntity();

        // Act
        int borrowId = GenerateInt();
        testBorrow.setBorrowId(borrowId);

        int bookId = GenerateInt();
        testBorrow.setBookId(bookId);

        int clientId = GenerateInt();
        testBorrow.setClientId(clientId);

        Date borrowDt = new Date(GenerateLong());
        testBorrow.setBorrowDt(borrowDt);

        Date returnDt = new Date(GenerateLong());
        testBorrow.setReturnDt(returnDt);

        BookEntity book = mock(BookEntity.class);
        when(book.getBookId()).thenReturn(bookId);

        ClientEntity client = mock(ClientEntity.class);
        when(client.getClientId()).thenReturn(clientId);

        // Assert
        assertEquals(client.getClientId(), testBorrow.getClientId());
        assertEquals(book.getBookId(), testBorrow.getBookId());

    }

}